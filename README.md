# README #

이 프로젝트는 단위테스트 프레임워크를 공부하기 위해 만든 프로젝트입니다.

### 무슨 내용이 있는가? ###

* JUnit 4.x의 기능 테스트
* Mockito의 기능 테스트

대단한 내용은 없으니 포킹도 불허합니다. ㅋㅋㅋ

참고 도서명 : Mastering Unit Testing Using Mockito and JUnit (Packt 출판사)