package com.packtpub.junit.recap;

import org.junit.experimental.theories.ParameterSignature;
import org.junit.experimental.theories.ParameterSupplier;
import org.junit.experimental.theories.PotentialAssignment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
public class RoleSupplier extends ParameterSupplier {
    @Override
    public List<PotentialAssignment> getValueSources(ParameterSignature parameterSignature) throws Throwable {
        List<PotentialAssignment> list = new ArrayList<>();
        list.add(PotentialAssignment.forValue("role", "아빠"));
        list.add(PotentialAssignment.forValue("role", "엄마"));
        list.add(PotentialAssignment.forValue("role", "딸"));
        list.add(PotentialAssignment.forValue("role", "아들"));
        return list;
    }
}
