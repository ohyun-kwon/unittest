package com.packtpub.junit.recap;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
public class Factorial {
    public long factorial(long number) {
        if (number == 0) {
            return 1;
        }
        return number * factorial(number - 1);
    }
}
