package com.packtpub.junit.recap;

import org.junit.experimental.theories.ParameterSignature;
import org.junit.experimental.theories.ParameterSupplier;
import org.junit.experimental.theories.PotentialAssignment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
public class NameSupplier extends ParameterSupplier {
    @Override
    public List<PotentialAssignment> getValueSources(ParameterSignature parameterSignature) throws Throwable {
        List<PotentialAssignment> list = new ArrayList<>();
        list.add(PotentialAssignment.forValue("name", "아빠 : 권오현"));
        list.add(PotentialAssignment.forValue("name", "엄마 : 이수정"));
        list.add(PotentialAssignment.forValue("name", "딸 : 권도원"));
        list.add(PotentialAssignment.forValue("name", "아들 : 권도훈"));
        return list;
    }
}
