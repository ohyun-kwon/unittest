package com.packtpub.junit.recap;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
public class Adder {
    public Object add(Number a, Number b) {
        return a.doubleValue() + b.doubleValue();
    }

    public Object add(String a, String b) {
        return a + b;
    }
}
