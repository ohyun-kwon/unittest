package com.packtpub.junit.recap.categories;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
@RunWith(Categories.class)
@Categories.IncludeCategory(CrazyTests.class)
@Categories.ExcludeCategory(SmartTests.class)
@Suite.SuiteClasses( { SomeTest.class, OtherTest.class })
public class OnlyCrazyTestSuite {
}
