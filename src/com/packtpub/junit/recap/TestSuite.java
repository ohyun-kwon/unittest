package com.packtpub.junit.recap;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ AssertTest.class, TestExecutionOrder.class,
        Assumption.class })
public class TestSuite {
}
