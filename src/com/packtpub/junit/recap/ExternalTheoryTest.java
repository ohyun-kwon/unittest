package com.packtpub.junit.recap;

import org.junit.experimental.theories.ParametersSuppliedBy;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
@RunWith(Theories.class)
public class ExternalTheoryTest {
    @Theory
    public void adds_numbers(
            @ParametersSuppliedBy(NumberSupplier.class) Number num1,
            @ParametersSuppliedBy(NumberSupplier.class) Number num2) {
        System.out.println(num1 + " and " + num2);
        Adder anAdder = new Adder();
        double expectedSum = num1.doubleValue() + num2.doubleValue();
        double actualResult = (Double) anAdder.add(num1, num2);
        assertEquals(expectedSum, actualResult, 0.01);
    }

    @Theory
    public void adds_string(
            @ParametersSuppliedBy(NameSupplier.class) String name,
            @ParametersSuppliedBy(RoleSupplier.class) String role) {
        if (name.startsWith(role)) {
            System.out.println(role + ", " + name);
            Adder anAdder = new Adder();
            assertEquals(anAdder.add(name, role), name + role);
        }
    }
}
