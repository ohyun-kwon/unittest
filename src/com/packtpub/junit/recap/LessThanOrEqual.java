package com.packtpub.junit.recap;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

/**
 * Created by iloveuu on 2017. 1. 19..
 */
public class LessThanOrEqual<T extends Comparable<T>> extends BaseMatcher<Comparable<T>> {
    private final Comparable<T> exptectedValue;

    public LessThanOrEqual(Comparable<T> exptectedValue) {
        this.exptectedValue = exptectedValue;
    }

    @Override
    public boolean matches(Object o) {
        int compareTo = exptectedValue.compareTo((T) o);
        return compareTo > -1;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format(" less than or equal(<=) %s", exptectedValue));
    }

    @Factory
    public static <T extends Comparable<T>> Matcher<T> lessThanOrEqual(T t) {
        return new LessThanOrEqual(t);
    }
}
