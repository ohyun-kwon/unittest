package com.packtpub.trading;

import com.packtpub.trading.dto.Stock;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

/**
 * Created by iloveuu on 2017. 1. 20..
 */
@RunWith(MockitoJUnitRunner.class)
public class StockBrokerTest {
    Map<String, List<Stock>> stockMap = new HashMap<>();
    @Mock
    MarketWatcher marketWatcher;
    @Mock
    Portfolio portfolio;
    StockBroker broker;

    @Before
    public void setUp() {
        broker = new StockBroker(marketWatcher);
    }

    @Test
    public void sanity() throws Exception {
        assertNotNull(marketWatcher);
        assertNotNull(portfolio);
    }

    @Test
    public void marketWatcher_Returns_current_stock_status() {
        Stock uvsityCorp = new Stock("UV", "Uvsity Corporation", new BigDecimal("100.00"));
        when(marketWatcher.getQuote(anyString())).thenReturn(uvsityCorp);
        assertNotNull(marketWatcher.getQuote("UV"));
        assertEquals("UV", marketWatcher.getQuote("UV").getSymbol());
    }

    @Test
    public void when_ten_percent_gain_then_the_stock_is_sold() {
//Portfolio's getAvgPrice is stubbed to return $10.00
        when(portfolio.getAvgPrice(isA(Stock.class))).
                thenReturn(new BigDecimal("10.00"));
//A stock object is created with current price $11.20
        Stock aCorp = new Stock("A", "A Corp", new BigDecimal("11.20"));
//getQuote method is stubbed to return the stock
        when(marketWatcher.getQuote(anyString())).thenReturn(aCorp);
//perform method is called, as the stock price increases
// by 12% the broker should sell the stocks
        broker.perform(portfolio, aCorp);
//verifying that the broker sold the stocks
        verify(portfolio).sell(aCorp, 10);
//        verify(portfolio, only()).sell(aCorp, 10);
    }

    @Test
    public void verify_zero_interaction() {
        verifyZeroInteractions(marketWatcher, portfolio);
    }

    @Test
    public void verify_no_more_interaction() {
        Stock noStock = null;
        portfolio.getAvgPrice(noStock);
        portfolio.sell(null, 0);
        verify(portfolio).getAvgPrice(eq(noStock));
//this will fail as the sell method was invoked
        verifyNoMoreInteractions(portfolio);
    }

    @Test
    public void argument_matcher() {
        when(portfolio.getAvgPrice(isA(Stock.class))).thenReturn(new BigDecimal("10.00"));
        Stock blueChipStock = new Stock("FB", "FB Corp", new BigDecimal(1000.00));
        Stock otherStock = new Stock("XY", "XY Corp", new BigDecimal(5.00));
        when(marketWatcher.getQuote(argThat(new BlueChipStockMatcher()))).thenReturn(blueChipStock);
        when(marketWatcher.getQuote(argThat(new OtherStockMatcher()))).thenReturn(otherStock);
        broker.perform(portfolio, blueChipStock);
        verify(portfolio).sell(blueChipStock, 10);
        broker.perform(portfolio, otherStock);
        verify(portfolio, never()).sell(otherStock, 10);
    }

    @Test(expected = IllegalStateException.class)
    public void throwsException() throws Exception {
        when(portfolio.getAvgPrice(isA(Stock.class))).thenThrow(new IllegalStateException("Database down"));
        portfolio.getAvgPrice(new Stock(null, null, null));
    }

    @Test(expected = IllegalStateException.class)
    public void throwsException_void_methods() throws Exception {
        doThrow(new IllegalStateException()).when(portfolio).buy(isA(Stock.class));
        portfolio.buy(new Stock(null, null, null));
    }

    @Test
    public void consecutive_calls() throws Exception {
        Stock stock = new Stock(null, null, null);
        when(portfolio.getAvgPrice(stock)).thenReturn(BigDecimal.TEN, BigDecimal.ZERO);
        assertEquals(BigDecimal.TEN, portfolio.getAvgPrice(stock));
        assertEquals(BigDecimal.ZERO, portfolio.getAvgPrice(stock));
        assertEquals(BigDecimal.ZERO, portfolio.getAvgPrice(stock));
    }

    @Test(expected = IllegalStateException.class)
    public void consecutive_calls2() {
        Stock stock = new Stock(null, null, null);
        when(portfolio.getAvgPrice(stock))
                .thenReturn(BigDecimal.TEN)
                .thenReturn(BigDecimal.TEN)
                .thenThrow(new IllegalStateException());

        assertEquals(BigDecimal.TEN, portfolio.getAvgPrice(stock));
        assertEquals(BigDecimal.TEN, portfolio.getAvgPrice(stock));
        portfolio.getAvgPrice(stock);
    }

    @Test
    public void answering() throws Exception {
        stockMap.clear();
        doAnswer(new BuyAnswer()).when(portfolio).buy(isA(Stock.class));
        when(portfolio.getCurrentValue()).then(new TotalPriceAnswer());
        portfolio.buy(new Stock("A", "A", BigDecimal.TEN));
        portfolio.buy(new Stock("B", "B", BigDecimal.ONE));
        assertEquals(new BigDecimal("11"), portfolio.getCurrentValue());
    }

    class TotalPriceAnswer implements Answer<BigDecimal> {
        @Override
        public BigDecimal answer(InvocationOnMock invocation) throws Throwable {
            BigDecimal totalPrice = BigDecimal.ZERO;
            for (String stockId : stockMap.keySet()) {
                for (Stock stock : stockMap.get(stockId)) {
                    totalPrice = totalPrice.add(stock.getPrice());
                }
            }
            return totalPrice;
        }
    }

    class BuyAnswer implements Answer<Object> {
        @Override
        public Object answer(InvocationOnMock invocation) throws Throwable {
            Stock newStock = (Stock) invocation.getArguments()[0];
            List<Stock> stocks = stockMap.get(newStock.getSymbol());
            if (stocks != null) {
                stocks.add(newStock);
            } else {
                stocks = new ArrayList<Stock>();
                stocks.add(newStock);
                stockMap.put(newStock.getSymbol(), stocks);
            }
            return null;
        }
    }
}

class BlueChipStockMatcher extends ArgumentMatcher<String> {
    @Override
    public boolean matches(Object symbol) {
        return "FB".equals(symbol) ||
                "AAPL".equals(symbol);
    }
}

class OtherStockMatcher extends BlueChipStockMatcher {
    @Override
    public boolean matches(Object symbol) {
        return !super.matches(symbol);
    }
}

class TestClass {}
class TestClass2 {}
class TestClass3 {}
class TestClass4 {}
