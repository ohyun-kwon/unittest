package com.packtpub.trading;

import com.packtpub.trading.dto.Stock;

/**
 * Created by iloveuu on 2017. 1. 20..
 */
public interface MarketWatcher {
    Stock getQuote(String symbol);
}
