package com.packtpub.trading.dto;

import java.math.BigDecimal;

/**
 * Created by iloveuu on 2017. 1. 20..
 */
public class Stock {
    private String symbol;
    private String companyName;
    private BigDecimal price;

    public Stock() {
    }

    public Stock(String symbol, String companyName, BigDecimal price) {
        this.symbol = symbol;
        this.companyName = companyName;
        this.price = price;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
