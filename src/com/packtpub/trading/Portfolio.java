package com.packtpub.trading;

import com.packtpub.trading.dto.Stock;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by iloveuu on 2017. 1. 20..
 */
public abstract class Portfolio {
    abstract public void buy(Stock stock);
    abstract public void sell(Stock stock, int qty);
    abstract public BigDecimal getAvgPrice(Stock stock);
    abstract public BigDecimal getCurrentValue();
    /*
    private Map<Stock, Integer> stockMap;

    public Portfolio() {
        stockMap = new HashMap<>();
    }

    public void buy(Stock stock) {
        if (stockMap.containsKey(stock))
            stockMap.put(stock, stockMap.get(stock) + 1);
        else
            stockMap.put(stock, 1);
    }

    public void sell(Stock stock, int qty) {
        if (stockMap.containsKey(stock)) {
            if (stockMap.get(stock) > 0)
                stockMap.put(stock, stockMap.get(stock) - qty);
            else
                throw new RuntimeException("주식 수가 모자람");
        } else
            throw new RuntimeException("보유하고 있지 않은 주식임");
    }

    public BigDecimal getAvgPrice(Stock stock) {
        if (!stockMap.containsKey(stock))
            throw new RuntimeException("보유하고 있지 않은 주식임");
        return new BigDecimal(stockMap.get(stock)).multiply(stock.getPrice());
    }
    */
}
